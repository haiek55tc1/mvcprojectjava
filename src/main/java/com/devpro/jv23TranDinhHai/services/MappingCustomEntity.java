package com.devpro.jv23TranDinhHai.services;

import com.devpro.jv23TranDinhHai.model.BaseEntity;

public interface MappingCustomEntity<E extends BaseEntity> {
	public E convert(Object[] data);
}
