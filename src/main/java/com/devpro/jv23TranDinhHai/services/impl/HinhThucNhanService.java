package com.devpro.jv23TranDinhHai.services.impl;

import org.springframework.stereotype.Service;

import com.devpro.jv23TranDinhHai.model.HinhThucEntity;
import com.devpro.jv23TranDinhHai.model.HinhThucNhanEntity;
import com.devpro.jv23TranDinhHai.services.BaseService;
@Service
public class HinhThucNhanService extends BaseService<HinhThucNhanEntity>{
	
	@Override
	protected Class<HinhThucNhanEntity> clazz() {
		// TODO Auto-generated method stub
		return HinhThucNhanEntity.class;
	}

}
