package com.devpro.jv23TranDinhHai.services.impl;

import org.springframework.stereotype.Service;

import com.devpro.jv23TranDinhHai.model.StatusEntity;
import com.devpro.jv23TranDinhHai.services.BaseService;

@Service
public class StatusService extends BaseService<StatusEntity> {
	
	@Override
	protected Class<StatusEntity> clazz() {
		// TODO Auto-generated method stub
		return StatusEntity.class;
	}

}
