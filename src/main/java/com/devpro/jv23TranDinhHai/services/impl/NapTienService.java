package com.devpro.jv23TranDinhHai.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.devpro.jv23TranDinhHai.dto.NapTienSearch;
import com.devpro.jv23TranDinhHai.model.NapTienEntity;
import com.devpro.jv23TranDinhHai.services.BaseService;
import com.devpro.jv23TranDinhHai.services.PagerData;



@Service
public class NapTienService extends BaseService<NapTienEntity>{
	
	@Override
	protected Class<NapTienEntity> clazz() {
		// TODO Auto-generated method stub
		return NapTienEntity.class;
	}
	
	
	
	
	
	public PagerData<NapTienEntity> napTienSearch(NapTienSearch napTienSearch){
		
		String sql = "select * from nap_tien where 1 = 1 ";
		
		if(napTienSearch != null) {
			if(napTienSearch.getStatusId() != null && !"0".equals(napTienSearch.getStatusId())) {
				sql += " and status_id = "+ napTienSearch.getStatusId();
			}else 
			
			if (!StringUtils.isEmpty(napTienSearch.getKeyword())) {
				sql += " and noi_dung like '%"+ napTienSearch.getKeyword()+"%'";
			}
		}
		
		System.out.println("sql: "+ sql);
		System.out.println("page number: "+ napTienSearch.getPage());
		
		
		return getEntitiesByNativeSQL(sql, napTienSearch.getPage());
	}
	
	


}
