package com.devpro.jv23TranDinhHai.dto;

public class ChuyenTienNoiBoDTO {
	private int moneyNumber;
	private String phoneNumber;
	private String name;
	public int getMoneyNumber() {
		return moneyNumber;
	}
	public void setMoneyNumber(int moneyNumber) {
		this.moneyNumber = moneyNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	

}
