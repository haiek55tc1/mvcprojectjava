package com.devpro.jv23TranDinhHai.dto;

import com.devpro.jv23TranDinhHai.services.BaseService;

public class UserSearch {
	
	private String keyword;
	private String statusId;
	private String currentPage;
	private String userSeo;
	
	
	public String getUserSeo() {
		return userSeo;
	}
	public void setUserSeo(String userSeo) {
		this.userSeo = userSeo;
	}
	public String getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	
	
	public Integer getPage() {
		
		try {
			return Integer.parseInt(this.getCurrentPage());
		} catch (Exception e) {
			// TODO: handle exception
			return BaseService.NO_PAGING;
		}
	}
	
	
	

}
