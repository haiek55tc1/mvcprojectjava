package com.devpro.jv23TranDinhHai.dto;

import org.springframework.stereotype.Controller;


public class LoginDTO {
	
	private int phoneNumber;
	private String pass;
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	

}
