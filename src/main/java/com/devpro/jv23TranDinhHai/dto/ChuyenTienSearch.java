package com.devpro.jv23TranDinhHai.dto;

public class ChuyenTienSearch {
	
	private String keyword;
	private String statusId;
	private String chuyenTienSeo;

	public String getChuyenTienSeo() {
		return chuyenTienSeo;
	}
	public void setChuyenTienSeo(String chuyenTienSeo) {
		this.chuyenTienSeo = chuyenTienSeo;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}


	

}
