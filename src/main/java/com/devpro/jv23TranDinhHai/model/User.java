	package com.devpro.jv23TranDinhHai.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Entity
@Table(name = "user_customer")
public class User extends BaseEntity implements UserDetails{
	
	@Column(name = "ho",length = 45, nullable = false)
	private String firstN;
	
	@Column(name = "ten",length = 45 ,nullable = false)
	private String ten;
	
	@Column(name = "money",precision = 13, scale = 2, nullable = true)
	private BigDecimal money;
	
	@Column(name = "seo",length = 100 ,nullable = true)
	private String seo;
	
	@Column(name = "address", length = 200, nullable = true)
	private String address;
	
	@Column(name = "job", length = 45, nullable = true)
	private String job;
	
	
	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSeo() {
		return seo;
	}

	public void setSeo(String seo) {
		this.seo = seo;
	}


	@DateTimeFormat (pattern = "yyy-mm-dd")
	@Column(name = "birthday", nullable = true)
	private Date birthday;
	
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}


	@Column(name = "email", nullable = false)
	private String email;



	@Column(name = "phone_number",length = 10 ,nullable = false)
	private int phoneNumber;


	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	@Column(name = "password",length = 200 ,nullable = false)
	private String password;
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "user_role",
	joinColumns = @JoinColumn(name = "user_id"),
	inverseJoinColumns = @JoinColumn(name = "role_id"))
	
	private Set<Role> roles = new HashSet<Role>();
	public void addRole(Role role) {
		role.getUsers().add(this);
		roles.add(role);
	}
	
	public void removeRole(Role role) {
		role.getUsers().remove(this);
		roles.remove(role);
	}
	
	
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	


	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "status_id")
	private StatusEntity statusEntioitys;
	
	
	public StatusEntity getStatusEntioitys() {
		return statusEntioitys;
	}

	public void setStatusEntioitys(StatusEntity statusEntioitys) {
		this.statusEntioitys = statusEntioitys;
	}


	@OneToMany(fetch = FetchType.EAGER,
			cascade = CascadeType.ALL,
			mappedBy = "users")
	
	private Set<HoSoEntity> hoSos = new HashSet<HoSoEntity>();//Dung der cho ben duoi lamf add va remove
	
	
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL,
			mappedBy = "user")
	
	private Set<ChuyenTienEntity> chuyenTienS = new HashSet<ChuyenTienEntity>();
	
	public void addChuyenTien(ChuyenTienEntity chuyenTien) {
		chuyenTien.setUser(this);
		chuyenTienS.add(chuyenTien);
	}
	
	public void removeChuyenTien(ChuyenTienEntity chuyenTien) {
		chuyenTien.setUser(null);
		chuyenTienS.remove(chuyenTien);
		
	}
	
	
	public Set<ChuyenTienEntity> getChuyenTienS() {
		return chuyenTienS;
	}

	public void setChuyenTienS(Set<ChuyenTienEntity> chuyenTienS) {
		this.chuyenTienS = chuyenTienS;
	}

	public void addImageHoSo(HoSoEntity hoSo) {
		hoSo.setUsers(this);
		hoSos.add(hoSo);
		
	}
	
	public void removeImageHoSo(HoSoEntity hoSo) {
		hoSo.setUsers(null);
		hoSos.remove(hoSo);
	}
	
	
	@OneToMany(fetch = FetchType.EAGER,
			cascade = CascadeType.ALL,
			mappedBy = "users")
	
	private Set<NapTienEntity> napTiens = new HashSet<NapTienEntity>();
	
	
	public void addNapTien(NapTienEntity napTien) {
		napTien.setUsers(this);
		napTiens.add(napTien);
	}
	
	public void removeNapTien(NapTienEntity napTien) {
		napTien.setUsers(null);
		napTiens.remove(napTien);
	}
	
	
	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Set<NapTienEntity> getNapTiens() {
		return napTiens;
	}

	public void setNapTiens(Set<NapTienEntity> napTiens) {
		this.napTiens = napTiens;
	}


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "category_id")
	private Categories categories;

	public Categories getCatgories() {
		return categories;
	}

	public void setCatgories(Categories catgories) {
		this.categories = catgories;
	}



	public void setFirstN(String firstN) {
		this.firstN = firstN;
	}

	public String getFirstN() {
		return firstN;
	}

	public void setFirst(String firstN) {
		this.firstN = firstN;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	public Set<HoSoEntity> getHoSos() {
		return hoSos;
	}

	public void setHoSos(Set<HoSoEntity> hoSos) {
		this.hoSos = hoSos;
	}


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return this.roles;
	}




	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}



}
