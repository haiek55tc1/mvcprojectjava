package com.devpro.jv23TranDinhHai.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "chuyen_tien")
public class ChuyenTienEntity extends BaseEntity{
	
	@Column(name = "nguoi_gui", nullable = true)
	private int phoneNguoiGui;
	
	@Column(name = "nguoi_nhan", length = 100, nullable = false)
	private String nameNguoiNhan;
	
	

	
	public int getPhoneNguoiGui() {
		return phoneNguoiGui;
	}

	public void setPhoneNguoiGui(int phoneNguoiGui) {
		this.phoneNguoiGui = phoneNguoiGui;
	}

	public String getNameNguoiNhan() {
		return nameNguoiNhan;
	}

	public void setNameNguoiNhan(String nameNguoiNhan) {
		this.nameNguoiNhan = nameNguoiNhan;
	}

	@Column(name = "seo", length = 200, nullable = true)
	private String seo;
	
	
	public String getSeo() {
		return seo;
	}

	public void setSeo(String seo) {
		this.seo = seo;
	}

	@Column(name = "money_gui",precision = 13, scale = 2, nullable = true)
	private BigDecimal moneyGui;
	
	
	@Column(name = "money_nhan",precision = 13, scale = 2, nullable = true)
	private BigDecimal moneyNhan;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "nhan_id")
	private HinhThucNhanEntity nhanEntity;
	
	
	public HinhThucNhanEntity getNhanEntity() {
		return nhanEntity;
	}

	public void setNhanEntity(HinhThucNhanEntity nhanEntity) {
		this.nhanEntity = nhanEntity;
	}

	public BigDecimal getMoneyGui() {
		return moneyGui;
	}

	public void setMoneyGui(BigDecimal moneyGui) {
		this.moneyGui = moneyGui;
	}

	public BigDecimal getMoneyNhan() {
		return moneyNhan;
	}

	public void setMoneyNhan(BigDecimal moneyNhan) {
		this.moneyNhan = moneyNhan;
	}

	@Column(name = "phone_nhan", nullable = false)
	private int phoneNhan;
	
	@Column(name = "so_ho_so", nullable = true)
	private String soHoSo;
	
	@Column(name = "code", nullable = true)
	private int code;
	
	
	@Column(name = "address", nullable = true)
	private String address;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "hinh_thuc_id")
	private HinhThucEntity hinhThucEntity;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private User user = new User();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "status_id")
	private StatusEntity statusEntity;

	public StatusEntity getStatusEntity() {
		return statusEntity;
	}

	public void setStatusEntity(StatusEntity statusEntity) {
		this.statusEntity = statusEntity;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getPhoneNhan() {
		return phoneNhan;
	}

	public void setPhoneNhan(int phoneNhan) {
		this.phoneNhan = phoneNhan;
	}



	public String getSoHoSo() {
		return soHoSo;
	}

	public void setSoHoSo(String soHoSo) {
		this.soHoSo = soHoSo;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public HinhThucEntity getHinhThucEntity() {
		return hinhThucEntity;
	}

	public void setHinhThucEntity(HinhThucEntity hinhThucEntity) {
		this.hinhThucEntity = hinhThucEntity;
	}
	
	
	
	
	

}
