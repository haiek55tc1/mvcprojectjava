package com.devpro.jv23TranDinhHai.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "nap_tien")
public class NapTienEntity extends BaseEntity{

	@Column(name = "noi_dung", nullable = false)
	private String noiDung;
	
	@Column(name = "money", precision = 13, scale = 2, nullable = false)
	private BigDecimal money;
	
	@Column(name = "seo", nullable = true)
	private String seo;
	
	public String getSeo() {
		return seo;
	}

	public void setSeo(String seo) {
		this.seo = seo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User users = new User();
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "status_id")
	
	private StatusEntity statusEntitys = new StatusEntity();
	
	

	public StatusEntity getStatusEntitys() {
		return statusEntitys;
	}

	public void setStatusEntitys(StatusEntity statusEntitys) {
		this.statusEntitys = statusEntitys;
	}

	public String getNoiDung() {
		return noiDung;
	}

	public void setNoiDung(String noiDung) {
		this.noiDung = noiDung;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public User getUsers() {
		return users;
	}

	public void setUsers(User users) {
		this.users = users;
	}

}
