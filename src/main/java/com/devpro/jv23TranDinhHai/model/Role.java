package com.devpro.jv23TranDinhHai.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

//Để có thẻ kết nối được với table của db ta cần 2 anotation là Entity và Table(name = "ten bang")

@Entity
@Table(name = "role")
public class Role extends BaseEntity implements GrantedAuthority{
	
	@Column(name = "role_name",length = 45 ,nullable = false)
	private String roleName;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "roles")
	private Set<User> users = new HashSet<User>();
	
	public void addUser(User user) {
		user.getRoles().add(this);
		users.add(user);
	}
	
	public void removeUser(User user) {
		user.getRoles().remove(user);
		users.remove(user);
	}
	

	public Set<User> getUsers() {
		return users;
	}


	public void setUsers(Set<User> users) {
		this.users = users;
	}


	public String getRoleName() {
		return roleName;
	}


	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return this.roleName;
	}
	

}
