package com.devpro.jv23TranDinhHai.model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "hinh_thuc_nhan")
public class HinhThucNhanEntity extends BaseEntity{
	@Column(name = "name", length = 45, nullable = false)
	private String nameHinhThuc;
	
	@Column(name = "ty_gia", nullable = false)
	private BigDecimal tyGia;
	
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL,
			mappedBy = "nhanEntity")
	private Set<ChuyenTienEntity> chuyenTienS = new HashSet<ChuyenTienEntity>();
	
	public void addChuyenTien(ChuyenTienEntity chuyenTien) {
		chuyenTien.setNhanEntity(this);
		chuyenTienS.add(chuyenTien);
	}
	
	public void removeChuyenTien(ChuyenTienEntity chuyenTien) {
		chuyenTien.setNhanEntity(null);
		chuyenTienS.remove(chuyenTien);
	}

	public String getNameHinhThuc() {
		return nameHinhThuc;
	}

	public void setNameHinhThuc(String nameHinhThuc) {
		this.nameHinhThuc = nameHinhThuc;
	}

	public BigDecimal getTyGia() {
		return tyGia;
	}

	public void setTyGia(BigDecimal tyGia) {
		this.tyGia = tyGia;
	}

	public Set<ChuyenTienEntity> getChuyenTienS() {
		return chuyenTienS;
	}

	public void setChuyenTienS(Set<ChuyenTienEntity> chuyenTienS) {
		this.chuyenTienS = chuyenTienS;
	}
	

}
