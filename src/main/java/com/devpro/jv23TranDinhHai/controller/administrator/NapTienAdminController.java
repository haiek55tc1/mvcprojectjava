package com.devpro.jv23TranDinhHai.controller.administrator;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.devpro.jv23TranDinhHai.controller.BaseController;
import com.devpro.jv23TranDinhHai.dto.NapTienSearch;
import com.devpro.jv23TranDinhHai.model.NapTienEntity;
import com.devpro.jv23TranDinhHai.model.StatusEntity;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.PagerData;
import com.devpro.jv23TranDinhHai.services.impl.NapTienService;
import com.devpro.jv23TranDinhHai.services.impl.StatusService;
import com.devpro.jv23TranDinhHai.services.impl.UserService;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;


@Controller
public class NapTienAdminController extends BaseController{
	
	
	@Autowired
	private StatusService statusService;
	
	@Autowired
	private NapTienService napTienService;
	
	@Autowired
	private UserService userService;
	

	
	@RequestMapping(value = {"/admin/nap-tien"}, method = RequestMethod.GET)
	public String napTien(final Model model, 
			final HttpServletRequest request,
			final HttpServletResponse response) throws IOException, IllegalStateException {
		List<StatusEntity> status = statusService.findAll();
		model.addAttribute("status", status);
		String currentPage = request.getParameter("currentPage");
		String keyword = request.getParameter("keyword");
		String statusId = request.getParameter("statusId");
		
		NapTienSearch napTienSearch = new NapTienSearch();
		napTienSearch.setCurrentPage(currentPage);
		napTienSearch.setKeyword(keyword);
		napTienSearch.setStatusId(statusId);
		
		PagerData<NapTienEntity> napTiens = napTienService.napTienSearch(napTienSearch);
		model.addAttribute("napTiens", napTiens);
		
		model.addAttribute("napTienSearch", napTienSearch);
		
		
		
		return "administrator/lichSuNapTien";
		
	}
	
	
	
	@RequestMapping(value = {"/admin/nap-tien/{seo}"}, method = RequestMethod.GET)
	public String confirmNapTien(final Model model, 
			final HttpServletRequest request,
			final HttpServletResponse response,
			@PathVariable("seo") String seo) throws IllegalStateException, IOException {
		
		String sql = "select * from nap_tien where seo = '"+seo+"'";
		
		NapTienEntity napTienSeo = napTienService.getEntityByNativeSQL(sql);
		
		sql = "select * from user_customer where phone_number = "+napTienSeo.getNoiDung();
		
		User userByPhone = userService.getEntityByNativeSQL(sql);
		BigDecimal money = userByPhone.getMoney().add(napTienSeo.getMoney());
		userByPhone.setMoney(money);
		
		
		StatusEntity statusConfirm =  statusService.getById(2);
		napTienSeo.setStatusEntitys(statusConfirm);
		
		userService.saveOrUpdate(userByPhone);
		
		return "redirect:/admin/nap-tien";
		
	}

}
