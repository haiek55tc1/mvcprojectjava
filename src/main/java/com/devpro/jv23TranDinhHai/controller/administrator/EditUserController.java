package com.devpro.jv23TranDinhHai.controller.administrator;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.devpro.jv23TranDinhHai.controller.BaseController;
import com.devpro.jv23TranDinhHai.dto.UserSearch;
import com.devpro.jv23TranDinhHai.model.HoSoEntity;
import com.devpro.jv23TranDinhHai.model.Role;
import com.devpro.jv23TranDinhHai.model.StatusEntity;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.impl.HoSoService;
import com.devpro.jv23TranDinhHai.services.impl.StatusService;
import com.devpro.jv23TranDinhHai.services.impl.UserService;

@Controller
public class EditUserController extends BaseController{
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private HoSoService hoSoServiec;
	
	@Autowired
	private StatusService statusService;
	

	
	
	@RequestMapping(value = {"/admin/register-post/{userSeo}"}, method = RequestMethod.GET)
	public String editUserSeo(
			final Model model,
			final HttpServletRequest request,
			final HttpServletResponse response, 
			@PathVariable("userSeo") String userSeo) throws IllegalStateException, IOException {
		
		UserSearch user = new UserSearch();
		user.setUserSeo(userSeo);
		User userBySeo = userService.userSearch(user).getData().get(0);
		model.addAttribute("user", userBySeo);
		
		HoSoEntity hoSo = hoSoServiec.getEntityByNativeSQL("select * from ho_so where id_user = "+
userBySeo.getId() + ";");	
		model.addAttribute("hoSo", hoSo);
		
		List<StatusEntity> statusList = statusService.findAll();
		model.addAttribute("status", statusList);
		
		return "administrator/editUser";
		
	}
	
	@RequestMapping(value = {"/admin/user/delete"}, method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> deleteUseer(
			final Model model,
			final HttpServletRequest request,
			final HttpServletResponse response,
			@RequestBody User user){
		
		User userId = userService.getById(user.getId());
		userId.setStatus(false);
		userService.saveOrUpdate(userId);
		
		Map<String, Object> jsonResult = new HashMap<String, Object>();
		jsonResult.put("code", 200);
		jsonResult.put("message", "Đã xoá thành công");
		
		return ResponseEntity.ok(jsonResult);
		
	}
	
	

}
