package com.devpro.jv23TranDinhHai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.devpro.jv23TranDinhHai.model.Categories;
import com.devpro.jv23TranDinhHai.model.Role;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.impl.CategoriesService;
import com.devpro.jv23TranDinhHai.services.impl.RoleService;

public class BaseController {
	
	@Autowired
	private CategoriesService categoriesService;
	@Autowired
	private RoleService roleServiec;
	
	
	@ModelAttribute("categories")
	public List<Categories> categoriesList() {
		return  categoriesService.findAll();
	}
	
	
	@ModelAttribute("roles")
	public List<Role> roles(){
		return roleServiec.findAll();
	}
	
	//Lấy ra thông tin user đã đăng nhập
	@ModelAttribute("userLogined")
	public User getUserLogined() {
		Object userLogined = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(userLogined != null && userLogined instanceof UserDetails) {
			return (User) userLogined;
		}
		
		return new User();
	}
	
	
	@ModelAttribute("isLogined")
	public boolean isLogined() {
		boolean isLogined = false;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(principal != null && principal instanceof UserDetails) {
			return true;
		}
		return isLogined;
	}

}
