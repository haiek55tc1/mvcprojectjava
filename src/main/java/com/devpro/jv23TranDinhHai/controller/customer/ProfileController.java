package com.devpro.jv23TranDinhHai.controller.customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.devpro.jv23TranDinhHai.controller.BaseController;

@Controller
public class ProfileController extends BaseController{
	
	@RequestMapping(value = {"/profile"}, method = RequestMethod.GET)
	
	private String profile(
			final Model model,
			final HttpServletRequest request,
			final HttpServletResponse response) {
		return "customer/profile";
	}

}
