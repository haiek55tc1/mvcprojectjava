package com.devpro.jv23TranDinhHai.controller.customer;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.devpro.jv23TranDinhHai.controller.BaseController;
import com.devpro.jv23TranDinhHai.dto.ChuyenTienSearch;
import com.devpro.jv23TranDinhHai.dto.FormNguoiNhanDTO;
import com.devpro.jv23TranDinhHai.model.ChuyenTienEntity;
import com.devpro.jv23TranDinhHai.model.HinhThucEntity;
import com.devpro.jv23TranDinhHai.model.HinhThucNhanEntity;
import com.devpro.jv23TranDinhHai.model.User;
import com.devpro.jv23TranDinhHai.services.impl.ChuyenTienServiec;
import com.devpro.jv23TranDinhHai.services.impl.HinhThucNhanService;
import com.devpro.jv23TranDinhHai.services.impl.HinhThucService;
import com.devpro.jv23TranDinhHai.services.impl.UserService;
import com.github.slugify.Slugify;

@Controller 
public class ChuyenTienHoController extends BaseController{
	
	@Autowired
	private HinhThucService hinhThucService;
	
	@Autowired
	private ChuyenTienServiec chuyenTienService;
	
	@Autowired
	private UserService userServiec;
	
	@Autowired 
	private HinhThucNhanService hinhThucNhanServicel;
	
	@RequestMapping(value = {"/chuyen-tien-ho"}, method = RequestMethod.GET)
	
	public String formNguoiGui(final Model model,
								final HttpServletRequest request,
								final HttpServletResponse response) {
		ChuyenTienEntity chuyenTien = new ChuyenTienEntity();
		
		model.addAttribute("formNguoiNhanDTO", chuyenTien);
		return "customer/formNguoiNhan";
	}
	
	
	@RequestMapping(value = {"/chuyen-tien-ho-post"}, method = RequestMethod.POST)
	public String nguoiGuiPost(final Model model,
								final HttpServletRequest request,
								final HttpServletResponse response,
								@ModelAttribute("formNguoiNhanDTO") ChuyenTienEntity chuyenTien) {
		
		
		HinhThucEntity hinhThucHo = hinhThucService.getById(1);
		chuyenTien.setHinhThucEntity(hinhThucHo);
		
		User user = userServiec.getById(25);
		chuyenTien.setUser(user);
		Slugify slugify = new Slugify();
		chuyenTien.setSeo(slugify.slugify(chuyenTien.getNameNguoiNhan()  + "-"+ System.currentTimeMillis()));
		
		chuyenTienService.saveOrUpdate(chuyenTien);
		
		
		return "redirect:/chuyen-tien-ho/"+chuyenTien.getSeo();
	}
	
	
	@RequestMapping(value = {"/chuyen-tien-ho/{napTienSeo}"}, method = RequestMethod.GET)
	public String senMonney(final Model model, 
			final HttpServletRequest request,
			final HttpServletResponse response,
			@PathVariable("napTienSeo") String napTienSeo) {
		
		ChuyenTienSearch chuyenTienSearch = new ChuyenTienSearch();
		chuyenTienSearch.setChuyenTienSeo(napTienSeo);
		model.addAttribute("chuyenTienSeo", chuyenTienSearch);
		return "customer/sendMoney";
		
	}
	
	
	@RequestMapping(value = {"/chuyen-tien-ho-post/{napTienSeo}"}, method = RequestMethod.POST)
	public String senMonneyPost(final Model model, 
			final HttpServletRequest request,
			final HttpServletResponse response,
			@PathVariable("napTienSeo") String napTienSeo) {
		
		String idNhan = request.getParameter("nhan_tien");
		String moneyNhan = request.getParameter("money_nhan");
		String moneyChuyen = request.getParameter("money_chuyen");
		
		String sql = "select * from chuyen_tien where seo = '"+napTienSeo+"'";
		System.out.println(sql);
		ChuyenTienEntity chuyenTienBySeo = chuyenTienService.getEntityByNativeSQL(sql);
		
		HinhThucNhanEntity nhanById = hinhThucNhanServicel.getById(Integer.parseInt(idNhan));
		chuyenTienBySeo.setNhanEntity(nhanById);
		chuyenTienBySeo.setMoneyGui(BigDecimal.valueOf(Double.parseDouble(moneyChuyen)));
		chuyenTienBySeo.setMoneyNhan(BigDecimal.valueOf(Double.parseDouble(moneyNhan)));

		
		chuyenTienService.saveOrUpdate(chuyenTienBySeo);
		return "redirect:/home";
		
	}
	
	
	

}
