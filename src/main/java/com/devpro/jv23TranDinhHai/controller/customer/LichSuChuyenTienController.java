package com.devpro.jv23TranDinhHai.controller.customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.devpro.jv23TranDinhHai.controller.BaseController;

@Controller
public class LichSuChuyenTienController extends BaseController{
	
	@RequestMapping(value = {"/lich-su-chuyen-tien"}, method = RequestMethod.GET)
	private String lichSu(
			final Model model,
			final HttpServletRequest request,
			final HttpServletResponse response) {
		return "customer/lichSuChuyenTien";
	}

}
