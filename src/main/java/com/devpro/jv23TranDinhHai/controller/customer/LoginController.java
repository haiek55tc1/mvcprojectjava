package com.devpro.jv23TranDinhHai.controller.customer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.devpro.jv23TranDinhHai.controller.BaseController;
import com.devpro.jv23TranDinhHai.dto.LoginDTO;

@Controller
public class LoginController extends BaseController{
	
	@RequestMapping(value = {"/login"}, method = RequestMethod.GET)
	private String login(final Model model,
						final HttpServletRequest request,
						final HttpServletResponse response) {
		
		return "customer/login";
	}
	
	@RequestMapping(value = {"/logout"}, method = RequestMethod.GET)
	private String logout(final Model model,
						final HttpServletRequest request,
						final HttpServletResponse response) {
		
		return "customer/login";
	}
	
}
