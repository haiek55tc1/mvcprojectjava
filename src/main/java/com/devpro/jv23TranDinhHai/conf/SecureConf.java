package com.devpro.jv23TranDinhHai.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.devpro.jv23TranDinhHai.services.impl.UserDetailServiceImp;


@Configuration
public class SecureConf extends WebSecurityConfigurerAdapter{
	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		//Hàm cấu hình tất cả các request từ trình duyệt
		http.csrf().disable().authorizeRequests()
		
		//Cho phép các request static resource không bị ràng buộc (permitAll)
		//Tức là các request trong ngoặc sẽ k bị chặn cũng như k bắt xác thực
		.antMatchers("/css/**", "/js/**", "/upload/**","/img/**", "/login", "/logout").permitAll()
		
		//Các kiểu reqeust: /admin/** phải xác thực (login thành công), mới được vào authenticated
		.antMatchers("/admin/**","/user/**").hasAnyAuthority("admin")		
		.and()
		
		//Nếu chưa xác thực thì hiển thị trang login
		//login: một request trong loginController
		.formLogin().loginPage("/login").loginProcessingUrl("/login_processing_url")
		.successHandler(authenticationSuccessHandler())
//		.defaultSuccessUrl("/home", true)//login thành công thì trở về trang home của người dùng
		.failureUrl("/login?login_error=true")//Đây là 1 cái cờ để đánh dấu nếu đăng nhập thất bại(sai pass/user)
		
		.and()
		
		
		//Cấu hình phần logout
		.logout().logoutUrl("/logout").logoutSuccessUrl("/home").invalidateHttpSession(true)
		.deleteCookies("JSESSSIONID");

		
	}
	
	@Autowired
	UserDetailServiceImp userDetailService;
	
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailService)
		.passwordEncoder(new BCryptPasswordEncoder(4));
	}
	
	@Bean
	public AuthenticationSuccessHandler authenticationSuccessHandler() {
		return new UrlAuthenticationSuccesshandler();
	}
	
	public static void main(String[] args) {
		System.out.println(new BCryptPasswordEncoder(4).encode("123456"));
		System.out.println(new BCryptPasswordEncoder(4).encode("pad123!@#"));
	}
	

}
