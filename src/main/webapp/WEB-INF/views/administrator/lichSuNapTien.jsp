
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- SPRING FORM -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>
    <jsp:include page="/WEB-INF/views/common/css.jsp"></jsp:include>
    <link rel="stylesheet" href="${base }/css/adminStyle.css" />
    <title>Danh sách tài khoản</title>
  </head>
  <body class="row">
    <nav style="height: 100vh;" class="col-3 h-auto bg-dark pr-0">
       <jsp:include page="/WEB-INF/views/common/navAdmin.jsp"></jsp:include>
    </nav>
    <main class="col-9 p-0">
      <header>
         <jsp:include page="/WEB-INF/views/common/headerAdmin.jsp"></jsp:include>
      </header>

<!-- content -->
      <form class="container mt-4">

        <!-- tìm kiếm -->
        <div class="category d-flex cursor-pointer">
            <input value = "${napTienSearch.currentPage }"  name = "currentPage" id = "currentPage" type = "text" class="rounded bg-transperant mr-2 fs-14 pl-2" placeholder="Page" />
          <input value = "${napTienSearch.keyword }" name = "keyword"  id = "keyword" class="rounded bg-transperant mr-2 fs-14 pl-2" type="text" placeholder="search">
          <select  name = "statusId" id = "statusId" class="fs-14 px-2 rounded mr-2" >
            <option value="${0 }">All</option>
            <c:forEach items = "${status }" var = "item">
            <option value="${item.id}">${item.statusName }</option>
            </c:forEach>
          </select>

          <button id = "btnSearch" type = "submit" class="border-0 bg-primary-color rounded fs-12 px-2" type="submit">Search</button>
          
        </div>

      <!--Tiêu đề   -->
      <div class="d-flex align-items-center bg-primary-color rounded-24 mt-4 py-2">
            <p class="w-5 text-center m-0 font-weight-bold">STT</p>
            <p class="w-25 text-center m-0 font-weight-bold">Thời gian</p>
            <p class="w-20 text-center m-0 font-weight-bold">Số điện thoại</p>
            <p class="w-25 text-center m-0 font-weight-bold">Số tiền</p>
            <p class="w-15 text-center m-0 font-weight-bold">Trạng thái</p>
            <p class="w-15 text-center m-0 font-weight-bold">Tuỳ chọn</p>
      </div>

      <!-- content -->
      <div>

      
      <c:forEach items = "${napTiens.data }" var = "napTien" varStatus = "loop">
              <!--  -->
        <div href="" class="d-flex align-items-center py-2 mt-2">
            <p class="w-5 text-center m-0 font-weight-light">${loop.index + 1}</p>
            <p class="w-25 text-center m-0 font-weight-light">${napTien.createdDate }</p>
             <p class="w-20 text-center m-0 font-weight-light">${napTien.noiDung }</p>
            <p class="w-25 text-center m-0 font-weight-light">
            <!-- định dạng tiền tệ -->
											<fmt:setLocale value="vi_VN" scope="session" />
											<fmt:formatNumber value="${napTien.money }" type="currency" /></p>
            <p class="w-15 text-center m-0 font-weight-light">${napTien.statusEntitys.statusName }</p>
            <c:if test='${napTien.statusEntitys.statusName } == "đang chờ" '>
               <a href="${base }/admin/nap-tien/${napTien.seo}" class="text-dark w-15 text-center m-0 d-flex bg-primary-color justify-content-center align-items-center rounded-24">          
                <p class="m-0">Xác nhận</p>
            </a>
            
            </c:if>
         
      </div>
      
      </c:forEach>
      </div>

      <!-- panigation -->
          <div class="row mt-4">
							<div class="col-12 d-flex justify-content-center">
								<div id="paging"></div>
							</div>
						</div> 
      <div class="d-flex justify-content-center mt-4">
          <p>panigation</p>
      </div>
    </form>
      </div>
     
    </main>

 <jsp:include page="/WEB-INF/views/common/js.jsp"></jsp:include>
 
 <script type="text/javascript">
 $( document ).ready(function() {
	 $("#statusId").val(${napTienSearch.statusId}); 
	$("#paging").pagination({
		currentPage: ${napTiens.currentPage}, 	//trang hiện tại
        items: ${napTiens.totalItems},			//tổng số sản phẩm
        itemsOnPage: ${napTiens.sizeOfPage}, 	//số sản phẩm trên 1 trang
        cssStyle: 'light-theme',
        onPageClick: function(pageNumber, event) {
        	$('#currentPage').val(pageNumber);
        	$('#btnSearch').trigger('click');
		},
    });
});
		</script>
  </body>
</html>
