
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- SPRING FORM -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>
    <jsp:include page="/WEB-INF/views/common/css.jsp"></jsp:include>
    <link rel="stylesheet" href="${base }/css/adminStyle.css" />

    <title>Admin Register</title>
  </head>
  <body class="row">
    <nav class="col-3 h-auto bg-dark pr-0">
      <jsp:include page="/WEB-INF/views/common/navAdmin.jsp"></jsp:include>
    </nav>
    <main class="col-9 pr-0">
      <header>
        <jsp:include page="/WEB-INF/views/common/headerAdmin.jsp"></jsp:include>
      </header>

<!-- content -->
      <div class="warpper pb-4">

        <div>
          <h4 class="text-center pt-4">Sửa tài khoản</h4>
          <sf:form modelAttribute="user" class="ml-100 mr-100" action="/admin/register-post" method = "post" enctype="multipart/form-data">

            <!--  -->
            <div class="fullname mt-4">
              <p class="h6 font-weight-light">Chọn loại tài khoản</p>
              <sf:select path = "" class="w-100 rounded border-input-reg py-2 pl-3" name="" id="">
                <sf:options items = "${roles }" itemValue = "id" itemLabel = "roleName"/>
              </sf:select>
            </div> 
            
            <!--  -->
            <div class="fullname mt-4">
              <p class="h6 font-weight-light">Trạng thái</p>
              <sf:select path = "status" class="w-100 rounded border-input-reg py-2 pl-3" name="" id="">
                <sf:options items = "${status }" itemValue = "id" itemLabel = "statusName"/>
              </sf:select>
            </div> 

             <!--  -->
             <div class="userId mt-4">
              <p class="h6 font-weight-light">ID </p>
              <sf:input path = "id" class="register__firstname w-100 rounded border-input-reg py-2 pl-3" type="text" name="user_id" placeholder="ID"></sf:input>
          </div>

            <!--  -->
            <div class="fullname mt-4">
               <p class="h6 font-weight-light">Nhập họ </p>
               <sf:input path = "firstN" class="register__firstname w-100 rounded border-input-reg py-2 pl-3" type="text"  placeholder="Họ đệm"></sf:input>
           </div>

            <!--  -->
            <div class="fullname mt-4">
               <p class="h6 font-weight-light">Nhập tên</p>
               <sf:input path = "ten" class="register__lastname w-100 rounded border-input-reg py-2 pl-3" type="text"  placeholder="Tên"></sf:input>
           </div>
            <!--  -->
            <div class="date mt-4">
               <p class="h6 font-weight-light">Sinh nhật</p>
               <sf:input path = "birthday" class="register__birthday w-100 rounded border-input-reg py-2 pl-3" type="date"></sf:input>
           </div>
               
           <!--  -->
               <div class="phone-number mt-4">
                   <p class="h6 font-weight-light">Số diện thoại</p>
                   <div class="d-flex align-items-center">
                       <p class="m-0 mr-4 w-10 border-input-reg py-2 px-4 rounded">+84</p>
                       <sf:input path = "phoneNumber" class="register__phone-number w-100 rounded border-input-reg py-2 pl-3" type="number"  placeholder="Số điện thoại"></sf:input>
                       
                   </div>
               </div>

               <!--  -->
               <div class="email mt-4">
                   <p class="h6 font-weight-light">Nhập địa chỉ enmail</p>
                   <sf:input path = "email" class="register__email w-100 rounded border-input-reg py-2 pl-3" type="email"  placeholder="Email"></sf:input>
               </div>
               
                              <!--  -->
               <div class="email mt-4">
                   <p class="h6 font-weight-light">Nhập địa chỉ</p>
                   <sf:input path = "address" class="register__email w-100 rounded border-input-reg py-2 pl-3" type="text"  placeholder="Hà Đông, Hà Nội"></sf:input>
               </div>
               
               
                              <!--  -->
               <div class="email mt-4">
                   <p class="h6 font-weight-light">Nhập nghề nghiệp</p>
                   <sf:input path = "job" class="register__email w-100 rounded border-input-reg py-2 pl-3" type="text"  placeholder="Tự do"></sf:input>
               </div> 

                 <!--  -->
                 <div class="password mt-4">
                   <p class="h6 font-weight-light">Nhập mật khẩu</p>
                   <sf:input path = "password" class="register__pass w-100 rounded border-input-reg py-2 pl-3" type="pasword"  placeholder="***************"></sf:input>
               </div>

                <!--  -->
                <div class="password__confirm mt-4">
                   <p class="h6 font-weight-light">Xác nhận mật khẩu</p>
                   <input class="register__pass-conf w-100 rounded border-input-reg py-2 pl-3" type="pasword"  placeholder="*****************">
               </div>

                 <!--  -->
                 <div class="select mt-4">
                   <p class="h6 font-weight-light">Chọn loại giấy tờ</p>
                  <sf:select path = "categories.id" class="w-100 rounded border-input-reg py-2 pl-3">
                      <sf:options items = "${categories }" itemValue = "id" itemLabel = "name" />
                  </sf:select>

                 <div class="d-flex align-items-center ">
                      <!-- Mặt trước -->
                  <input name = "matTruoc" type="file" class="register__mat-truoc d-none select-register mt-2" name="" id="file_truoc" accept="imgage/*">
                  <label for="file_truoc" class=" mr-2 bg-primary-color d-flex justify-content-center align-items-center rounded-24 mt-2 py-1" style="width: 14%;">
                      <div><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                       <g clip-path="url(#clip0_1423_8225)">
                       <path d="M17.6562 11.8615C17.4652 11.8615 17.3124 12.0143 17.3124 12.2053V15.3326C17.3124 16.1832 16.6197 16.8734 15.7716 16.8734H8.22835C7.37776 16.8734 6.68761 16.1807 6.68761 15.3326V12.1543C6.68761 11.9633 6.5348 11.8105 6.3438 11.8105C6.1528 11.8105 6 11.9633 6 12.1543V15.3326C6 16.5627 7.00085 17.561 8.22835 17.561H15.7716C17.0017 17.561 18 16.5601 18 15.3326V12.2053C18 12.0168 17.8472 11.8615 17.6562 11.8615Z" fill="white"/>
                       <path d="M10.0578 9.2128L11.6571 7.61348V14.5889C11.6571 14.7799 11.8099 14.9327 12.0009 14.9327C12.1919 14.9327 12.3447 14.7799 12.3447 14.5889V7.61348L13.944 9.2128C14.0102 9.27901 14.0994 9.31467 14.1859 9.31467C14.2751 9.31467 14.3617 9.28156 14.4279 9.2128C14.5629 9.07782 14.5629 8.86135 14.4279 8.72638L12.2428 6.54132C12.1792 6.47765 12.09 6.43945 12.0009 6.43945C11.9092 6.43945 11.8226 6.47511 11.7589 6.54132L9.57389 8.72638C9.43891 8.86135 9.43891 9.07782 9.57389 9.2128C9.70632 9.34523 9.92533 9.34523 10.0578 9.2128Z" fill="white"/>
                       </g>
                       <rect x="0.5" y="0.5" width="23" height="23" rx="11.5" stroke="white"/>
                       <defs>
                       <clipPath id="clip0_1423_8225">
                       <rect width="12" height="12" fill="white" transform="translate(6 6)"/>
                       </clipPath>
                       </defs>
                       </svg>
                       </div>
                      <p class="text-white m-0 pl-2">Mặt trước</p>
                  </label>

                   <!-- Mặt sau -->
                <input name = "matSau" type="file" class="register__mat-sau d-none select-register mt-2" name="" id="file_sau" accept="imgage/*">
                <label for="file_sau" class="bg-primary-color d-flex justify-content-center align-items-center rounded-24 mt-2 py-1" style="width: 14%;">
                    <div><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                     <g clip-path="url(#clip0_1423_8225)">
                     <path d="M17.6562 11.8615C17.4652 11.8615 17.3124 12.0143 17.3124 12.2053V15.3326C17.3124 16.1832 16.6197 16.8734 15.7716 16.8734H8.22835C7.37776 16.8734 6.68761 16.1807 6.68761 15.3326V12.1543C6.68761 11.9633 6.5348 11.8105 6.3438 11.8105C6.1528 11.8105 6 11.9633 6 12.1543V15.3326C6 16.5627 7.00085 17.561 8.22835 17.561H15.7716C17.0017 17.561 18 16.5601 18 15.3326V12.2053C18 12.0168 17.8472 11.8615 17.6562 11.8615Z" fill="white"/>
                     <path d="M10.0578 9.2128L11.6571 7.61348V14.5889C11.6571 14.7799 11.8099 14.9327 12.0009 14.9327C12.1919 14.9327 12.3447 14.7799 12.3447 14.5889V7.61348L13.944 9.2128C14.0102 9.27901 14.0994 9.31467 14.1859 9.31467C14.2751 9.31467 14.3617 9.28156 14.4279 9.2128C14.5629 9.07782 14.5629 8.86135 14.4279 8.72638L12.2428 6.54132C12.1792 6.47765 12.09 6.43945 12.0009 6.43945C11.9092 6.43945 11.8226 6.47511 11.7589 6.54132L9.57389 8.72638C9.43891 8.86135 9.43891 9.07782 9.57389 9.2128C9.70632 9.34523 9.92533 9.34523 10.0578 9.2128Z" fill="white"/>
                     </g>
                     <rect x="0.5" y="0.5" width="23" height="23" rx="11.5" stroke="white"/>
                     <defs>
                     <clipPath id="clip0_1423_8225">
                     <rect width="12" height="12" fill="white" transform="translate(6 6)"/>
                     </clipPath>
                     </defs>
                     </svg>
                     </div>
                    <p class="text-white m-0 pl-2">Mặt sau</p>
                </label>
             </div>
                 </div>

               </div>


               <div class="img d-flex w-100 h-100 mt-4 mb-4 align-items-center justify-content-center">
                <img src="${base }/upload/${hoSo.matTruoc}" class="w-25 h-25 mr-100" alt="">
                <img src="${base }/upload/${hoSo.matSau}" class="w-25 h-25" alt="">
              </div>
    

               
               <!-- button -->

               <a href="" class="m-0 d-flex justify-content-center">
                   <button type="submit" class="register__btn bg-primary-color border-0 text-white text-center rounded-24 w-25 py-2 cursor-pointer outline-none">

                       <p class="m-0" style="color: #FFFFFF;" >
                           Tiếp tục
                       </p>
                   </button>
               </a>
           </sf:form>
        </div>
      </div>
    </main>

      <jsp:include page="/WEB-INF/views/common/js.jsp"></jsp:include>
  </body>
</html>
