  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
  <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>
   <div
          class="bg-white box-shadow-col d-flex justify-content-end pr-50 align-items-center py-3"
        >



         <!--  -->
         <div class="w-10 h-10">
            <div 
       
              class="btn dropdown-toggle"
              href="#"
             
              id="dropdownMenuLink"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <img
                style="border-radius: 50%"
                class="w-30 h-80"
                src="${base }/imgs/avtAdmin.png"
                alt=""
              />
            </div>



            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="#">Profile</a>
                <a class="dropdown-item" href="#">Setting</a>
                <a class="dropdown-item" href="#">Logout</a>
              </div>
          </div>


          <!--  -->
          <div class="pr-4 pt-2 position-relative">
            <div>
              <svg
                width="18"
                height="15"
                viewBox="0 0 18 15"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g clip-path="url(#clip0_2489_1360)">
                  <path
                    d="M8.9991 0C8.28794 0 7.71339 0.418945 7.71339 0.9375V1.46191C4.80044 1.79883 2.57053 3.63867 2.57053 5.85938V6.83789C2.57053 8.16797 1.94776 9.45996 0.810709 10.4971L0.212048 11.0449C-0.0209877 11.2559 -0.0651842 11.5459 0.0955301 11.7891C0.256244 12.0322 0.593744 12.1875 0.963387 12.1875H17.0348C17.4045 12.1875 17.742 12.0322 17.9027 11.7891C18.0634 11.5459 18.0192 11.2559 17.7862 11.0449L17.1875 10.5C16.0504 9.45996 15.4277 8.16797 15.4277 6.83789V5.85938C15.4277 3.63867 13.1978 1.79883 10.2848 1.46191V0.9375C10.2848 0.418945 9.71026 0 8.9991 0ZM8.9991 2.8125H9.32053C11.6268 2.8125 13.4991 4.17773 13.4991 5.85938V6.83789C13.4991 8.24121 14.0576 9.60938 15.0942 10.7812H2.90401C3.94062 9.60938 4.4991 8.24121 4.4991 6.83789V5.85938C4.4991 4.17773 6.37142 2.8125 8.67767 2.8125H8.9991ZM11.5705 13.125H8.9991H6.42767C6.42767 13.623 6.69687 14.1006 7.17901 14.4521C7.66116 14.8037 8.31607 15 8.9991 15C9.68214 15 10.337 14.8037 10.8192 14.4521C11.3013 14.1006 11.5705 13.623 11.5705 13.125Z"
                    fill="black"
                  />
                </g>
                <defs>
                  <clipPath id="clip0_2489_1360">
                    <rect width="18" height="15" fill="white" />
                  </clipPath>
                </defs>
              </svg>
            </div>

            <div class="position-absolute noti-header">0</div>
          </div>


         
        </div>