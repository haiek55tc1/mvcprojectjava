<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <jsp:include page = "/WEB-INF/views/common/variables.jsp"></jsp:include>
    <jsp:include page="/WEB-INF/views/common/css.jsp"></jsp:include>
    <title>Lá»ch sá»­ chuyá»n tiá»n</title>
</head>
<body>

    <header>
        <jsp:include page = "/WEB-INF/views/common/header.jsp"></jsp:include>

    </header>
    <main class="mt-100 mb-100">
        <div class="container">
            <div class="category d-flex cursor-pointer">
                <p class="mr-4 font-weight-light border-bottom--primary-color pb-2" style="font-size: 14px;">Táº¥t cáº£ cÃ¡c giao dá»ch</p>
                <p class="mr-4 font-weight-light pb-2" style="font-size: 14px;">ÄÃ£ chuyá»n</p>
                <p class="mr-4 font-weight-light pb-2" style="font-size: 14px;">Äang xá»­ lÃ½</p>
                <p class="mr-4 font-weight-light pb-2" style="font-size: 14px;">HoÃ n thÃ nh</p>
                
            </div>

          <!--TiÃªu Äá»   -->
          <div class="d-flex align-items-center bg-primary-color rounded-24 mt-4 py-2">
                <p class="w-5 text-center m-0 font-weight-bold">STT</p>
                <p class="w-20 text-center m-0 font-weight-bold">Thá»i gian</p>
                <p class="w-20 text-center m-0 font-weight-bold">Sá» tiá»n</p>
                <p class="w-25 text-center m-0 font-weight-bold">NgÆ°á»i nháº­n</p>
                <p class="w-15 text-center m-0 font-weight-bold">Tráº¡ng thÃ¡i</p>
                <p class="w-15 text-center m-0 font-weight-bold">Chuyá»n thÃªm tiá»n</p>
          </div>

          <!-- content -->
          <div>
            <!--  -->
            <div href="" class="d-flex align-items-center py-2 mt-2">
                <p class="w-5 text-center m-0 font-weight-light">01</p>
                <p class="w-20 text-center m-0 font-weight-light">10/02/2023</p>
                <p class="w-20 text-center m-0 font-weight-light">10000000000000</p>
                <p class="w-25 text-center m-0 font-weight-light">Tráº§n ÄÃ¬nh Háº£i</p>
                <p class="w-15 text-center m-0 font-weight-light">HoÃ n thÃ nh</p>
                <a href="${base }/chuyen-tien-noi-bo" class="text-dark w-15 text-center m-0 d-flex bg-primary-color justify-content-center align-items-center rounded-24">
                    <div class="d-flex align-items-center mr-2"><svg width="18" height="18" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12 5V19" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M5 12H19" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        </div>
                    <p class="m-0">Chuyá»n thÃªm</p>
                </a>
          </div>

             <!--  -->
             <div href="" class="d-flex align-items-center py-2 mt-2">
                <p class="w-5 text-center m-0 font-weight-light">02</p>
                <p class="w-20 text-center m-0 font-weight-light">10/02/2023</p>
                <p class="w-20 text-center m-0 font-weight-light">10,000,000,000,000</p>
                <p class="w-25 text-center m-0 font-weight-light">Tráº§n ÄÃ¬nh Háº£i</p>
                <p class="w-15 text-center m-0 font-weight-light">HoÃ n thÃ nh</p>
                <a href="" class="text-dark w-15 text-center m-0 d-flex bg-primary-color justify-content-center align-items-center rounded-24">
                    <div class="d-flex align-items-center mr-2"><svg width="18" height="18" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12 5V19" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M5 12H19" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        </div>
                    <p class="m-0">Chuyá»n thÃªm</p>
                </a>
          </div>



             <!--  -->
             <div href="" class="d-flex align-items-center py-2 mt-2">
                <p class="w-5 text-center m-0 font-weight-light">03</p>
                <p class="w-20 text-center m-0 font-weight-light">10/02/2023</p>
                <p class="w-20 text-center m-0 font-weight-light">10,000,000,000,000</p>
                <p class="w-25 text-center m-0 font-weight-light">Tráº§n ÄÃ¬nh Háº£i</p>
                <p class="w-15 text-center m-0 font-weight-light">Äang xá»­ lÃ½</p>
                <a href="" class="text-dark w-15 text-center m-0 d-flex bg-primary-color justify-content-center align-items-center rounded-24">
                    <div class="d-flex align-items-center mr-2"><svg width="18" height="18" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12 5V19" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M5 12H19" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        </div>
                    <p class="m-0">Chuyá»n thÃªm</p>
                </a>
          </div>

          </div>

          <!-- panigation -->
          <div class="d-flex justify-content-center mt-4">
              <p>panigation</p>
          </div>
        </div>
    </main>
    <footer></footer>
    
    <jsp:include page="/WEB-INF/views/common/js.jsp"></jsp:include>
</body>
</html>